## IIC2344 - Laboratorio de Arquitectura de Computadores ##
## Experiencia 3: Tarjeta SD ##

Los archivos contenidos en la carpeta `src` tienen las siguentes funcionalidades:

+ `main_sd.c`    : Punto de partida para escribir la aplicacion.
+ `HAL_SDCard.c` : Funciones para manejar el hardware de la tarjeta SD, principalmente los metodos para el protocolo SPI, etc.
+ `mmc.c`        : Libreria para manejar el protocolo SD/MMC, como leer y escribir bloques.
+ `ff.c`         : Libreria de FatFS para manejar sistemas de archivos en la tarjeta SD.
+ `ffconf.h`     : Configuracion de sistema de archivos y otros relevantes para la tarjeta SD.
