#include <msp430.h>
int main(void) {
	WDTCTL = WDTPW + WDTHOLD; // Paramos WDT
	P1SEL &= ~BIT0;      // Pin 1.0 como IO 
	P1DIR |= BIT0;       // Pin 1.0 como salida
	P1OUT |= BIT0;       // Pin 1.0 en 1
	P8SEL &= ~(BIT1 | BIT2);      // Pin 1.0 como IO 
	P8DIR |= (BIT1 | BIT2);       // Pin 1.0 como salida
	P8OUT |= (BIT1 | BIT2);       // Pin 1.0 en 1
	LPM3;                // Modo de bajo consumo
	return 0;
}
