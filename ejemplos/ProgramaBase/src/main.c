#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <msp430.h>

#include "led.h"
#include "timer.h"
#include "button.h"

void use_dco() {
	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
	// Set DCO to 1MHz
	UCSCTL4 |= SELM__DCOCLK;
}

void msp_init(void) {
	use_dco();	
	__enable_interrupt();
}

int main(void) {
	msp_init();
	led_init();
	button_init();
	timer_init();
	timer_start();
	_NOP();
	LPM0;
}

void msp_reset() {
	WDTCTL = 0;
}
