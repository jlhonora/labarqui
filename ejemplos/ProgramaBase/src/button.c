#include "button.h"
void button_init(void) {
        BUTTON1_IE  &= ~BUTTON1_BIT; // Disable interrupts while setting up
        BUTTON1_DIR &= ~BUTTON1_BIT; // Treat as input (P1DIR7 = 0)
        BUTTON1_IES |=  BUTTON1_BIT; // Falling edge interrupt (1)
        BUTTON1_OUT |=  BUTTON1_BIT; // Pull up
        BUTTON1_REN |=  BUTTON1_BIT; // Pull up enabled
        BUTTON1_IFG &= ~BUTTON1_BIT;
        BUTTON1_SEL &= ~BUTTON1_BIT; // I/O function
        BUTTON1_IE  |=  BUTTON1_BIT; // Enable interrupt
}

//In uniarch there is no more signal.h to sugar coat the interrupts definition, so we do it here
#ifndef interrupt
#define interrupt(x) void __attribute__((interrupt (x))) 
#endif
interrupt (PORT1_VECTOR) PORT1_ISR()
{
	if (BUTTON1_IFG & BUTTON1_BIT)
	{
		// Put interrupt code here
		TA0CTL ^= ID__2;

		// Tell the microcontroller that the interrupt has been processed
		BUTTON1_IFG &= ~BUTTON1_BIT;
	}
}
