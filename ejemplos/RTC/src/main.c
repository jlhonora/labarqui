#include "msp430.h"
#include "rtc.h"
#include "app.h"

void init() {
  	WDTCTL = WDTPW + WDTHOLD;
	UCSCTL4 |= SELM_3;
	UCSCTL5 = 0x00;
  	_BIS_SR(GIE);
}

TIME_DATA RTC_get_time(void) {
	TIME_DATA data;
	data.year = RTCYEAR;
	data.month = RTCMON;
	data.day  = RTCDAY;
	data.hour = RTCHOUR;
	data.minute = RTCMIN;
	data.second = RTCSEC;
	return data;	
}

// This sould be implemented somewhere else
extern int16_t LCD_printf(const char * fmt, ...);
extern void LCD_init(void);

extern volatile TIME_DATA last_time_data;
void app_run() {
	LCD_init();
	LCD_printf("Init");
	LCD_printf("\rRTC init");
	RTC_init();
	LCD_printf("\rRunning");
	TIME_DATA data;
	while(1) {
		P8DIR |= BIT1;
		P8OUT ^= BIT1;
		data = RTC_get_time();
		if(data.second != last_time_data.second) {
			LCD_print_time(data);
			last_time_data = data;
		}
		__delay_cycles(1000);
	}
}

void RTC_seconds_handler() {
	LCD_print_time(last_time_data);
	P8DIR |= BIT2;
	P8OUT |= BIT2;
	__delay_cycles(200);
	P8OUT &= ~BIT2;
}

void LCD_set_pointer(uint8_t, uint8_t);
void LCD_print_time(TIME_DATA data) {
	LCD_set_pointer(0,0);
	LCD_printf("%u/%02u/%02u\n", data.year, data.month, data.day);
	LCD_printf("%02u:%02u:%02u", data.hour, data.minute, data.second);
}

void RTC_event_handler() {

}

int main (void)
{
	init();
	app_run();
	while(1);
	return 0;
}
