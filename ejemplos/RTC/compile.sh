filename="rtc_init_default.h"
output="src/rtc_init.h"
processDelay=6 #seconds

cp $filename $output
a=$(date +%Y)
sed -i "s/DYEAR/$a/g" $output
a=$(date +%m)
sed -i "s/DMON/$a/g" $output
a=$(date +%d)
sed -i "s/DDAY/$a/g" $output
a=$(date +%H)
sed -i "s/DHOUR/$a/g" $output
a=$(date +%M)
sed -i "s/DMIN/$a/g" $output
a=$(($(date +%S) + $processDelay))
sed -i "s/DSEC/$a/g" $output

rake
sudo mspdebug rf2500 "prog bin.elf"
