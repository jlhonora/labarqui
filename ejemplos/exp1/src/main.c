#include <msp430.h>
#include "lcd.h"
#include "test.h"

void init() {
	WDTCTL = WDTPW | WDTHOLD;
  	__delay_cycles(3000);
	LCD_init();
  	LCD_backlightInit();
	LCD_setAllPixelsOn();
}

int main (void) {
	init();
	test();
	while(1);
	return 0;
}
