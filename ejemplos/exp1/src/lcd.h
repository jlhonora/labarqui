/*******************************************************************************
 *
 *  lcd.h
 *
 *  Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#ifndef HAL_DOGS102X6_H
#define HAL_DOGS102X6_H

#include <stdint.h>

// Screen size
#define DOGS102x6_X_SIZE   102         // Display Size in dots: X-Axis
#define DOGS102x6_Y_SIZE    64         // Display Size in dots: Y-Axis

// Screen printing styles
#define DOGS102x6_DRAW_NORMAL   0x00   // Display dark pixels on a light background
#define DOGS102x6_DRAW_INVERT   0x01   // Display light pixels on a dark background

extern uint8_t dogs102x6Memory[];      // Provide direct access to the frame buffer

extern void LCD_init(void);
extern void LCD_backlightInit(void);
extern void LCD_disable(void);
extern void LCD_writeCommand(uint8_t* sCmd, uint8_t i);
extern void LCD_writeData(uint8_t* sData, uint8_t i);
extern void LCD_setAddress(uint8_t pa, uint8_t ca);
extern uint8_t LCD_getContrast(void);
extern uint8_t LCD_getBacklight(void);
extern void LCD_setContrast(uint8_t newContrast);
extern void LCD_setBacklight(uint8_t brightness);
extern void LCD_setInverseDisplay(void);
extern void LCD_clearInverseDisplay(void);
extern void LCD_scrollLine(uint8_t lines);
extern void LCD_setAllPixelsOn(void);
extern void LCD_clearAllPixelsOn(void);
extern void LCD_clearScreen(void);
extern void LCD_charDraw(uint8_t row, uint8_t col, uint16_t f, uint8_t style);
extern void LCD_charDrawXY(uint8_t x, uint8_t y, uint16_t f, uint8_t style);
extern void LCD_stringDraw(uint8_t row, uint8_t col, char *word, uint8_t style);
extern void LCD_stringDrawXY(uint8_t x, uint8_t y, char *word, uint8_t style);
extern void LCD_clearRow(uint8_t row);
extern void LCD_pixelDraw(uint8_t x, uint8_t y, uint8_t style);
extern void LCD_horizontalLineDraw(uint8_t x1, uint8_t x2, uint8_t y, uint8_t style);
extern void LCD_verticalLineDraw(uint8_t y1, uint8_t y2, uint8_t x, uint8_t style);
extern void LCD_lineDraw(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t style);
extern void LCD_circleDraw(uint8_t x, uint8_t y, uint8_t radius, uint8_t style);
extern void LCD_imageDraw(const uint8_t IMAGE[], uint8_t row, uint8_t col);
extern void LCD_clearImage(uint8_t height, uint8_t width, uint8_t row, uint8_t col);
// Adds printf functionality to the LCD. If the end of the LCD
// is reached then the lines must be scrolled
int16_t LCD_printf(const char * fmt, ...);
int LCD_putchar(int character);
void LCD_set_pointer(uint8_t row, uint8_t col);
void LCD_clear(void);
#endif /* HAL_DOGS102x6_H */
