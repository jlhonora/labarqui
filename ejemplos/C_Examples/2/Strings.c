#include <stdio.h>
#include <string.h>

/* Parte I
int main(void) {
	char str[32];
	strcpy(str, "Test string\r\n");
	printf("%s", str);
}*/

/*// Parte II
int main(void) {
	char * str;
	strcpy(str, "Test string\r\n");
	printf("%s", str);
}*/

/*// Parte III
int main(void) {
	char * str = "Test string\r\n";
	printf("%s", str);
}*/

// Parte IV
int main(void) {
	char * str = "Test string";
	char str2[32];
	char str3[32];

	sprintf(str2, "%s + extra", str);
	memcpy(str3, str2, (int) strlen(str2));
	memcpy(str3, "123456789", 5);

	printf("%s\r\n", str2);
	printf("%s\r\n", str3);
}
