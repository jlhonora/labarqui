#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <msp430.h>
#include "uart.h"

#define DEBUG(...) do {uart_printf(__VA_ARGS__);} while(0);

void led_init() {
	P1SEL &= ~BIT0;
	P1DIR |= BIT0;
	P1OUT |= BIT0;
}

void init() {
	WDTCTL = WDTPW + WDTHOLD;
	led_init();
	uart_init();
	__enable_interrupt();
}

int main (void)
{
	init();
	while(1) {
		DEBUG("1");
	}
	return 0;
}
